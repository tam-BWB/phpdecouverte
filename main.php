<?php
    echo "coucou \n";
    $a = 1;
    echo $a . " est la bonne valeur \n {$a} \n";

    //Création tableau vide
    $a = array();
    //Ajouter élement au tableau dans la position 1
    $a[0]="toto";
    //Ajouter élement au tableau dans la position 2
    $a["first"] = "tutu";
    //Affichage
    echo $a[0] . "\n";
    echo $a["first"] . "\n";
    print_r($a);
?>