<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
<section>
<?php
    /*EXO 1 Dans une section afficher 10 articles*/
    /*sleep(10);
    for ($i=0; $i < 10; $i++) { 
        echo "<article>toto{$i}</article>";
    }*/


    /*EXO 2 */
    $arr = ["toto","tutu","titi","tata","tutu"];
    
    /*Avec le tableau précédent, afficher les valeurs dans des articles*/
    for ($i=0, $size = count($arr); $i < $size; $i++) { 
        echo "<article>$arr[$i]</article>";
    }
    /*Initialiser 2 tableaux de longueur 12 (title, contents) avec lesquels on pleupera des articles*/
    $title = ["title1","title1","title1","title1","title1","title1","title1","title1","title1","title1","title1","title1"];
    $contents = ["title1","title1","title1","title1","title1","title1","title1","title1","title1","title1","title1","title1"];

    /*$rows = 11;
    $cols= 1;
    function creerTable($rows,$cols){
        echo "<table border='1'>";
        echo "<th>Valeur</th>";
        for ($tr=0; $tr < $rows ; $tr++) { 
            echo "<tr>";
            for ($td=0; $td < $cols; $td++) { 
                //echo "<td>".$tr*$td."</td>";
                echo "<td><article></article></td>";
            }
            echo "</tr>";
        }
        echo "</table>";
    }
    creerTable(11,1);
    creerTable(11,1);*/

    /*Initialiser 3 tableaux de longueur 12 (title, contents, footers) avec lesquels on pleupera des articles*/
    $title = ["title1","title1","title1","title1","title1","title1","title1","title1","title1","title1","title1","title1"];
    $contents = ["title1","title1","title1","title1","title1","title1","title1","title1","title1","title1","title1","title1"];
    $footers = ["title1","title1","title1","title1","title1","title1","title1","title1","title1","title1","title1","title1"];
    
    /*$rows = 10;
    $cols= 1;
    function creerTables($rows,$cols){
        echo "<table border='1'>";
        echo "<th>Valeur</th>";//thead>tr>th
        for ($tr=0; $tr < $rows ; $tr++) { 
            echo "<tr>";
            for ($td=0; $td < $cols; $td++) { 
                //echo "<td>".$tr*$td."</td>";
                echo "<td><article></article></td>";
            }
            echo "</tr>";
        }
        echo "<td>Total</td>";//tfoot>tr>td
        echo "</table>";
    }
    creerTables(10,1);
    creerTables(10,1);
    creerTables(10,1);*/

    /*Afficher 3 articles par ligne*/
    $rows = 10;
    $cols= 1;
    function creerTables2($rows,$cols){
        $arr = ["toto","tutu","titi","tata","tutu"];
        echo "<table border='1'>";
        echo "<th>Valeur</th>";//thead>tr>th
        for ($tr=0; $tr < $rows ; $tr++) { 
            echo "<tr>";
            for ($td=0; $td < $cols; $td++) { 
                //echo "<td>".$tr*$td."</td>";
                for ($i=0, $size = count($arr); $i < $size; $i++) { 
                    echo "<td><article>$arr[$i]</article></td>";
                }
            }
            echo "</tr>";
        }
        echo "<td>Total</td>";//tfoot>tr>td
        echo "</table>";
    }
    creerTables2(10,1);

    /*Afficher 4 articles par ligne */

    /*Afficher 2 articles par ligne */

?>
</section>
</body>
</html>